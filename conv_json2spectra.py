#!/usr/bin/env python3
"""
 The only purpose is to open the last output file
and extract data from it..
"""
from fire import Fire
import glob
import json
import datetime as dt
import time
import sys
import pandas as pd

from tabulate import tabulate # panda print
import numpy as np

def save_exo( name, exonp ):
    """

    """
    if exonp is None:
        return

    filename = f"hpge_{name}.asc1"
    print( "i... saving numpy exo1/2:", filename )
    np.savetxt( filename, exonp, delimiter="\n", fmt='%d')


def save_txt( name, spe):
    """
    takes spe (list or numpy array) and saves on column
    used when json file is extracted...
    """
    filename = name+".txt"
    filename = name+".asc1"
    print(f"i... saving {filename} ")
    #return

    with open( filename , "w" ) as f:
        for i in spe:
            f.write( f"{i}\n")



def compose( name = None ):
    """
    create-extract   txt/asc1 spectra from /json/ result
    """
    name = "dask_results_log*.json"
    print(f"i... composing {name}")
    dd = sorted( glob.glob( name ) )
    if len(dd)<=0:
        print(f"X... no {name}")
        sys.exit(0)

    print(dd)
    print()
    print("i.. opening the last JSON",dd[-1])
    #dic = json.load( dd[-1] )
    with open(dd[-1], "r") as f:
        dic = json.load( f )
    print(dic.keys())

    extr = {}
    n =0
    once = True
    for i in dic.keys(): # FOR ALL RUNS

        if n==0:print( f"{i} .. {dic[i]}")
        n+=1
        lis = dic[i]

        # --extraction of something
        extr[i] = lis[2] # EXTRACT THIS - length


        # spectrum is DICT # 2 ?
        dic2 = lis[3] # SPECTRUM ON 3
        if type(dic2) is dict:
            print( lis[0], lis[1], dic2.keys()  ) # PRints keys
            exogam1 = None
            exogam2 = None
            # - j IS RUN LIKE ---
            for j in dic2.keys():
                exo = 0
                if int(j.split("_")[-1]) in [4,5,6,7]:
                    exo = 1
                    if exogam1 is None:
                        exogam1 = np.array( dic2[j] ) # from numpy
                    else:
                        exogam1 = exogam1 + np.array( dic2[j] ) # from numpy

                if int(j.split("_")[-1]) in [8,9,10,11]:
                    exo = 2
                    if exogam2 is None:
                        exogam2 = np.array( dic2[j] ) # from numpy
                    else:
                        exogam2 = exogam2 + np.array( dic2[j] ) # from numpy

                ru,nu,cr = j.split("_")
                save_txt( f"{ru}_{int(nu):03d}_{int(cr):02d}", dic2[j] )
                # --- this was in one run

            runname = j.split("_")[1]
            runname = int(runname)
            save_exo( f"{runname:03d}_S27", exogam1)
            save_exo( f"{runname:03d}_S26", exogam2)
        else:
            if once:
                print("X... on position 2 - there is no dict with spectra")
                once = False

    #print(extr)
    #df = pd.Series(extr)
    #print(tabulate(df, headers='keys', tablefmt='psql'))
    #    #pd.set_option("display.max_rows", None, "display.max_columns", None)
    # PRINT ALL
    #with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
    #    print(df)


def main():
    print()

if __name__=="__main__":
    Fire(compose)
