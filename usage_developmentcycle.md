
## Development line:

 * install the package in development mode in your environment with all requirements:
     - `pip3 install -e .`
     - remove with `pip3 uninstall project`

 * standard procedure to commit and eventually tag versions
  ```
  git commit -a
  bumpversion patch
  # bumpversion minor
  # bumpversion release
  ```

 * prepare distribution and upload to PyPI
  ```
   ./distcheck
   #SUGGESTIONS:
   # TEST
   # twine upload --repository-url https://test.pypi.org/legacy/ dist/*
   # install back from PyPI
   # pip3 install --index-url https://test.pypi.org/simple/ nuphy
   # pip3 install --index-url https://test.pypi.org/simple/ nuphy  --upgrade
   # ======= REAL CASE =============
   #     bumpversion release
   # twine upload --repository-url https://pypi.org/ dist/*
   # pip3 install daskcheck
   # pip3 install  daskcheck  --upgrade
