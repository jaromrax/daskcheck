#!/usr/bin/env python3

# to override print <= can be a big problem with exceptions
from __future__ import print_function # must be 1st
import builtins

import sys

from fire import Fire

from daskcheck.version import __version__
from daskcheck import unitname
from daskcheck import config



from daskcheck.daskcheck import submit, get_cpu_info, prepare_params
from daskcheck.daskcheck import announce
from daskcheck.daskcheck import proper_function_main

from console import fg,bg,fx

def test(server = None):
    """
    HARDWIRED TEST - 40X get_cpu_info submited to cluster
    """
    NTASKS = 40
    submit(  get_cpu_info, [ str(x) for x in range(1,NTASKS) ] , server=server)

# ...................................................................
def proper(filename, PARAMS, server = None):
    """
    give filename.py, that will be imported and where main() will be called in cluster
    """
    parameters = prepare_params(PARAMS)
    announce(f"module {filename};   \nparameter list: {parameters}", color = f"{bg.mediumvioletred}{fg.white}")
    #return
    #NTASKS = 40
    # submit(  proper_function_info, [ x for x in range(1,NTASKS) ] , upload="~/Downloads/beam_dump.pdf" ,server=server)
    # submit(  proper_function_main, [ x for x in parameters ] , upload="singlemod.py" ,server=server)
    submit(  proper_function_main, [ x for x in parameters ] , upload=filename ,server=server)

# ...................................................................
# ...................................................................
# ...................................................................
# ...................................................................

def loc():
    """
    Run the test get_cpu  locally.
    """
    print(  get_cpu_info( 1, "par1")  )



#==============================================================================================
#==============================================================================================
#==============================================================================================
if __name__=="__main__":
    print("i...  Daskcheck (DC) -  loc for get_cpu_info; test for submit job")
    Fire({"net":test,
          "loc":loc,
          "file":proper
    } )
