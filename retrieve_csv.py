#!/usr/bin/env python3
"""
CSV extract column with result
"""
from fire import Fire

import csv
import ast
import sys

def main( filename):
    """
    extract column 'res'  from csv file and use as dict and save to spe
    """
    print()
    csv.field_size_limit(sys.maxsize)
    column_name = 'res'

    with open(filename, mode='r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:


            # do what you want with the result column
            # Convert the string representation of a dictionary to an actual dictionary
            dictionary = ast.literal_eval(row[column_name])

            for key, values in dictionary.items():
                # Create a file for each key
                with open(f'{key}.txt', mode='w') as file:
                    for value in values:
                        # Write each value on a new line
                        file.write(f'{value}\n')

if __name__=="__main__":
    Fire(main)
