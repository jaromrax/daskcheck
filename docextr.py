#!/usr/bin/env python3

from fire import Fire
from console import fg,bg,fx

def main():
    """
    I would like to have ONE GLOBAL SCRIPT to operate module's main()

    attemt to see how GolbalScript can be generated automatically - f.py
    with docstrings  to react on Fire
    ... problem with default parameters
    ... maybe the hard way - cutting () from original file...
    """
    import importlib
    from importlib import reload

    print()
    di = {"sinf":"singlemod.main"}
    for k,v in di.items():
        bare = f"{v.split('.')[0]}"
        fun = f"{v.split('.')[1]}"
        xcore = importlib.import_module(f'{bare}')
        #print(f"{fg.black}{bg.white} {bare} {fun} {fg.default}{bg.default}")

        pars = xcore.main.__doc__.split("\n")
        pars = [x.strip() for x in pars if x.find(":param ")>0]
        pars = [ x.split(":param ")[1] for x in pars]
        pars = [ x.split(":")[0] for x in pars ]


        print(f"def {k}({','.join(pars)}):")
        #print('    """')
        print('    """',xcore.main.__doc__)
        print('    """')
        #print(pars)
        print(f"    {v}({','.join(pars)})")
        #main.__doc__=xcore.main.__doc__
        #help(main)
if __name__=="__main__":
    Fire(main)
