#!/bin/bash

echo do some tricks  collection hgere
echo ===== 2023/12 ===
#
#

USER=`whoami`
if [ "$USER" = "root" ]; then
    echo X... DO NOT USE AS ROOT...
    echo X... else you will mess up at some moment
    exit 1
fi


# READ SIZE OF TERM
read -r rows cols < <(stty size)
((cols=cols+2))
while [ 1 ]; do

CHOICES=$(whiptail --separate-output --checklist "Choose options" 20 $cols 14 \
		       --cancel-button "EXIT" \
    "sums" "sums of all txt and asc1 files" OFF \
    "quit" "Quit" OFF 3>&1 1>&2 2>&3)

if [ -z "$CHOICES" ]; then
    echo "X... No option was selected (user hit Cancel or unselected all options)"
    #sleep 1
    exit 2
else
  for CHOICE in $CHOICES; do
    case "$CHOICE" in
    "sums")
	echo "$CHOICE"

	for file in *.{txt,asc1}; do
	    echo -en "$file: \t ";
	    awk '{sum+=$1} END {print sum}' "$file";
	done

	echo ================== DONE
	echo Enter
	read ans
	;;

        "Quit")
            break
	    echo ================== DONE
            ;;


        *) echo "invalid option $REPLY";;
    esac
  done
fi

done
exit 0
