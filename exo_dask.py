#!/usr/bin/env python3

from daskcheck import daskcheck

from fire import Fire
import time
import platform
import datetime as dt
import json


# <2023-01-10 Tue 16:18> started all runs read
# <2023-01-10 Tue 17:26> the last one on gigajm done

def main( parlist ):
    parameters = daskcheck.prepare_params( parlist )

    if type(parameters)==list:
        print("i... viable for DASK ....")
        daskcheck.submit( xcorefunc  ,  parameters )
        #daskcheck.submit( daskcheck.get_cpu_info , parameters)
    else:
        print("i... running only locally")
        my_results = xcorefunc( 1 , parameters )
        # Write LOG file.
        now = dt.datetime.now()
        stamp = now.strftime("%Y%m%d_%H%M%S")
        with open(f"dask_results_log_{stamp}.json", "w") as fp:
            json.dump( my_results , fp, sort_keys=True, indent='\t', separators=(',', ': '))
    return



def xcorefunc( order, param):
    """
    Function to be sent to dask server with order# + parameter
    """
    import ROOT # I need to avoid breaking pickle
    start_time = time.perf_counter()
    print()
    length = 2**15

    name = f"run_{param:d}" # RUN NAME LINE run_113

    cha = ROOT.TChain("TreeMaster")
    print("i.. adding root files into the chain...")
    nas_server = "http://147.231.102.62"
    server_addr = "/WEBDATA/20221003_ganil_nfs_deuterons_molybden/02_exogam_root_files/RooT"
    cha.Add(f"{nas_server}{server_addr}/{name}_r0.root")
    cha.Add(f"{nas_server}{server_addr}/{name}_r1.root")
    cha.Add(f"{nas_server}{server_addr}/{name}_r2.root")
    cha.Add(f"{nas_server}{server_addr}/{name}_r3.root")
    cha.Add(f"{nas_server}{server_addr}/{name}_r4.root")
    cha.Add(f"{nas_server}{server_addr}/{name}_r5.root")
    cha.Add(f"{nas_server}{server_addr}/{name}_r6.root")
    cha.Add(f"{nas_server}{server_addr}/{name}_r7.root")
    cha.Add(f"{nas_server}{server_addr}/{name}_r8.root")

    # --------- taken from the previous table @ lookatroot.org
    # S26 is 8..11
    # S27 is 4..7
    calibs = {
        4: ( 0.22187053, -0.02733429 ),
        5: ( 0.22331387 , 0.07867),
        6: ( 0.22262142,0.10096038  ),
        7: ( 0.23095473, 0.29864468 ),
        8: ( 0.21150070, 0.1461 ),
        9: (0.22161151, 0.25350),
        10: (0.21905076,0.4540 ),
        11: (0.22326532,0.21057447 )

        }
        #4: (0.17979, 222.68), 5: (0.18101, 222.44), 6: (0.18024, 223.56), 7: (0.18709, 223.14), 8: (0.17139, 222.39), 9: (0.1795, 222.97), 10: (0.17752, 222.45), 11: (0.18087, 223.06)}


    # --------- create ihstograms --------------
    hdir = {}
    for i in range(4,12):
        hname = f"{name}_{i:02d}"
        hdir[hname] = ROOT.TH1F(f"{hname}", f"{hname}", length, 0, length )
        #print(hname)
        print(" ... creating histo ",hdir[hname])
    print()

    # PARSE CHAIN -----------------------
    ni=0
    for event in cha:
        #if ni>100000: break # NO PREMATURE BREAK
        if ni%10000 == 0:  print(f"{ni/1000:8.0f} kEvts   ", end="\r")
        ni+=1

        if  len(event.fEXO_ECC_E_Energy)>=2:
            # check  the crystal  and fill the proper histogram
            for evi in range( len(event.fEXO_ECC_E_Energy) ):
                crystal = int(event.fEXO_ECC_E_DetNbr[evi])
                # I take Calibration from previous analysis and do factor 2x (0.5 keV channel)
                a = calibs[crystal][0]
                b = calibs[crystal][1]
                hdir[f"{name}_{crystal:02d}"].Fill( 2*(a*float(  event.fEXO_ECC_E_Energy[evi]  )+b) )
    print()

    # ------ create LISTtxt with the spectra ----------------AND DICTIONARY
    spectra = {}
    for key in hdir.keys():
        #with open(f"{key}.txt", "w") as f:
        content = []
        for j in range(1, hdir[key].GetNbinsX() ):
            con = int(hdir[key].GetBinContent(j))
            content.append(con)
            #f.write( f"{con}\n" )
        spectra[key] = content

    return order, [platform.node(),  f"{time.perf_counter() - start_time:.1f} s" , ni, spectra]





if __name__=="__main__":
    Fire(main)
